//En el store creamos el contexto para poder utilizarlo en diferentes componentes 
import React, { Component, createContext } from 'react';
const { Provider, Consumer } = createContext();
//provider solo se encargara de englobar toda la aplicacion .
class ContextStore extends Component {
    //creamos una funcion para cambiar el state y la pasamos como props, esto la hace metodo. 
    //la creamos arriba del estado para que funcione. Ya que en el estado pasamos una funcion que previamente debe ser creada.
    changeName = () => {
        this.setState({
            user: {
                name: 'Peter tomson'
            }
        });

    }
    //podemos pasar la data creando un objeto state:
    state = {

        user: { name: 'john Doe' },
        product: { name: 'cama', price: 24 },

        changeName: this.changeName
    }

    // y pasar ese state como value.


    render() {
        return (
            <Provider value={this.state}>
                {/* otra forma no muy correcta de pasar el objeto en provider
                <Provider value={{ user: { name: 'john Doe' }, product: { name: 'cama', price: 24 } }}>
            */}

                {//el comp es el componente que le mandamos desde  el index route
                    //el componente que yo le pase en pla prop comp , va a ser envuelto
                    /* por el provider y accedera a value.
                    asi mismo los componenten que esten dentro de ese accederan 
                    por medio de <Consumer> que siempre devuelve value*/
                }
                {this.props.comp}

            </Provider>

        )
    }
}

export default Consumer;
export { ContextStore };

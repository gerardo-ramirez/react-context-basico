import React, { Component, Fragment } from 'react';
//cada vez que queramos usar la data del store usamos Consumer;
import Consumer from './../store';

class User extends Component {
    render() {
        return (
            <Fragment>


                Users page
                    <h1>Hola usuario:  </h1>
                <Consumer>
                    {
                        ({ user, changeName }) => (

                            <div>
                                <h3>Name: {user.name}</h3>
                                <button onClick={changeName}>ChangeName</button>
                            </div>
                        )
                    }
                </Consumer>




            </Fragment>
        )
    }

}
export default User; 
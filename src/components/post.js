import React, { Component } from 'react';
import Consumer from '../store';

class Post extends Component {
    render() {
        return (
            <div>
                Posts page
                <Consumer>
                    {
                        value => (
                            <p>el producto es : {value.product.name}</p>
                        )
                    }
                </Consumer>
            </div>

        )
    }

}
export default Post; 
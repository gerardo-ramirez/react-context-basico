import React, { Component, } from 'react';
//hacemos sun rutearos:
import { Link } from 'react-router-dom';


import Consumer from './../store';
class Toolbar extends Component {
    //el consumer nos trae informacion o las propiedades puestas en el provider.
    render() {
        return (

            <Consumer>
                {
                    value => (
                        <div>
                            <h2>Bienvenido {value.user.name}</h2>
                            <Link to='/user'>Users</Link>
                            <br />
                            <Link to='/post'>Posts</Link>
                            <br />
                            <Link to='/'>Home</Link>



                        </div>
                    )
                }
            </Consumer>
        )
    }
};
export default Toolbar;

import React, { Component } from 'react';
import Consumer from './../store';

class Piloto extends Component {
    render() {
        return (

            <Consumer>{
                value => (
                    <div>
                        <p> {value.product.name}</p>
                        <p> {value.product.price}</p>

                    </div>

                )
            }

            </Consumer>

        )
    }

}
export default Piloto;
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import App from '../App';
import Post from '../components/post';
import User from '../components/user';
//en lugar de llamar el contexto aqui lo llamamos del store:
import { ContextStore } from './../store';

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/post" render={props => <ContextStore comp={<Post />} />} />
                <Route exact path="/user" render={props => <ContextStore comp={<User />} />} />
                <Route exact path="/" render={props => <ContextStore comp={<App />} />} />
            </Switch>

        </BrowserRouter>
    )
}
export default Routes

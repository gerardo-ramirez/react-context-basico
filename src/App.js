import React, { Component, Fragment } from 'react';

import Piloto from './components/piloto';

import Toolbar from './components/toolbar';




class App extends Component {
  render() {
    return (

      <Fragment>
        <Toolbar />
        <Piloto />
      </Fragment>
    );
  }

}

export default App;
